# Richiesta

Descrivere il più dettagliatamente possibile l’esecuzione di una pagina Web che ha per codice sorgente il file index.html

# Analisi dell'applicazione

- la pagina contiene 3 blocchi script
  - il primo codice si pone immediatamente dopo il tag body ed è richiamato quasi prima del DOM (non è in HEAD)
  - il secondo codice è definito "inline"
  - il terzo è una closure, caricato dopo il DOM

## Il primo blocco di codice
    function prova (a, b) {
      this.somma = a + b;
    }
- dichiara una funzione globale
- "somma" è disponibile per il suo context
    - quindi è pubblicamente disponibile quando chiamato globalmente
          es. "prova(2, 3)" -> window.somma == 5;
    - è interno al tag <a> se chiamato dal codice inline
    - aggiorna "obj.somma" all'interno della sua closure se richiamato tramite "obj.fn(3, 4)"

## Il codice inline
    
    prova.call(this, 1, 2);
    
- viene eseguito al volo quando necessario
- richiama "prova" tramite la funzione Function.prototype.call()
  - sarebbe utile per passare un context diverso da "this"
  - il context this è in questo caso globale

## La closure

    var obj = {};
    obj.fn = prova;
    prova(2, 3);
    obj.fn(3, 4);

- le sue variabili sono racchiuse e non sono accessibili al resto dell'environment
- prime due righe di codice collegano la funzione "prova" ad uno scope all'interno di un oggetto
- la terza richiama la funzione "prova" in modo globale
- l'ultima riga richiama "prova" attraverso l'alias precedentemente dichiarato (è sempre la stessa funzione)

## Altre note

- "somma" è disponibile per il suo context
    - quindi è pubblicamente disponibile quando chiamato globalmente
          es. "prova(2, 3)" // window.somma == 5;
    - aggiorna "obj.somma" all'interno della sua closure se richiamato tramite "obj.fn(3, 4)"

Analisi dell'applicazione fornita.

- la pagina contiene 3 blocchi script
    - il primo codice si pone immediatamente dopo il tag body, quindi viene caricato prima del DOM (quasi, non è in HEAD)
    - il secondo codice è definito "inline"
    - il terzo è una closure, caricato dopo il DOM

# Il primo blocco di codice
    function prova (a, b) {
      this.somma = a + b;
    }
- dichiara una funzione globale

# Il codice inline
    
    prova.call(this, 1, 2);
    
- viene eseguito al volo quando necessario
- richiama "prova" tramite la funzione Function.prototype.call()
    - sarebbe utile per passare un context diverso da "this"
    - il context this è in questo caso globale

# La closure

    var obj = {};
    obj.fn = prova;
    prova(2, 3);
    obj.fn(3, 4);

- le sue variabili sono racchiuse e non sono accessibili al resto dell'environment
- prime due righe di codice collegano la funzione "prova" ad uno scope all'interno di un oggetto
- la terza richiama la funzione "prova" in modo globale
- l'ultima riga richiama "prova" attraverso l'alias precedentemente dichiarato (è sempre la stessa funzione)

# Altre note

Il codice ha una serie di bad practices (ne cito per brevità solo alcuni)
    - "prova" è dichiarato globalmente anzichè in uno scope come "Matematica.prova()"
    - bad naming di variabili e funzioni
    - è preferibile evitare il codice inline
    - mancanza di separazione tra html e script, meglio se in file diversi e richiamati da fondo pagina
    - cliccando il codice inline il browser muta scroll per via del "#" e mancanza di preventDefault
    - utilizzo di un context troppo generico "this.somma", meglio "window.ultima_somma" se proprio


